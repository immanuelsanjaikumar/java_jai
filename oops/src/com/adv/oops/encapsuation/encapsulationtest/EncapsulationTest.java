package com.adv.oops.encapsuation.encapsulationtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.adv.oops.encapsulation.SubClass;
import com.adv.oops.encapsulation.SuperClass;

public class EncapsulationTest extends SuperClass {
		private SubClass SubCls = new SubClass();
		//private ProtectedCallingForTest ProtCallForTest=new ProtectedCallingForTest();

		@Test
		public void EncapTestPublic() {
			String actualResult = SubCls.Publi("Imman");
			String expectedResult = "Imman";
			assertEquals(expectedResult, actualResult);
		}
		
		/*@Test
		public void EncapTestPrivate() {
			String actualResult = SubCls.protectPrivate("Private");
			String expectedResult = "Private";
			assertEquals(expectedResult, actualResult);
		}*/
		@Test
		public void EncapTestProtected() {
			EncapsulationTest encaps=new  EncapsulationTest();
			String actualResult = encaps.Protct("Protected");
			String expectedResult = "Protected";
			assertEquals(expectedResult, actualResult);
		}
	}
