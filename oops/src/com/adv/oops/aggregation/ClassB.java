package com.adv.oops.aggregation;

public class ClassB {
	public String ClassBPrimiMeth(String name) {// Method With Argument as Primitive DataType
		System.out.println("Primitive Class B " + name);
		return name;
	}
}
