package com.adv.oops.aggregation;

public class ClassA {

	private ClassB clsB;

	private int age;// Primitive DataType

	public ClassA(ClassB clsB) {// Constructor With Argument as Non-Primitive DataType
		this.clsB = clsB;
	}

	public ClassA(int age) {// Constructor With Argument as Primitive DataType
		this.age = age;
	}

	public String ClassAPrimiMeth(String name) {// Method With Argument as Primitive DataType
		System.out.println("Primitive Class A " + name);
		return name;
	}

//	public ClassB ClassANonPrimitMeth(ClassB clsB) {// Method With Argument as Non-Primitive DataType
//		System.out.println("Non-Primitive Class A " + clsB.ClassBPrimiMeth("Called"));
//		return clsB;
//	}

	public void setClsB(ClassB clsB) {// Setter With Argument as Non-Primitive DataType
		this.clsB = clsB;
	}

	public ClassB getClsB() {// Getter for Non-Primitive DataType
		return clsB;
	}

	public void setAge(int age) {// Setter With Argument as Primitive DataType
		this.age = age;
	}

	public int getAge() {// Getter for Primitive DataType
		return age;
	}
}
