package com.adv.oops.aggregation;

public class AggregationTest {

	public static void main(String[] args) {
		ClassB clsBT = new ClassB();
		ClassA clsAT = new ClassA(clsBT);// Passing Object reference as arguments (setting Class B
											// object Reference to clsB(Variable)
		clsAT.ClassAPrimiMeth("Called");
		clsAT.setAge(22);
		ClassB clB = clsAT.getClsB();
		clB.ClassBPrimiMeth("Called After Getted");
		System.out.println(clsAT.getAge());
		clsAT = null;
		System.out.println(clsAT.getAge());

	}

}
