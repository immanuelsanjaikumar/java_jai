package com.adv.oops.aggregation.aggregationtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.adv.oops.aggregation.ClassA;
import com.adv.oops.aggregation.ClassB;

public class AggregationTest {

	private ClassB clsBT = new ClassB();
	private ClassA clsAT = new ClassA(clsBT);
	private ClassB clB = clsAT.getClsB();

	@Test
	public void aggregationTest() {

		String actualResult = clsAT.ClassAPrimiMeth("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);

	}

	@Test
	public void aggregationRefTest() {

		String actualResult = clB.ClassBPrimiMeth("Called After Getted");
		String expectedResult = "Called After Getted";
		assertEquals(expectedResult, actualResult);

	}

	@Test(expected = NullPointerException.class)
	public void aggregationReffTest() {
		clsAT = null;
		int age = clsAT.getAge();
		int actualResult = age;

	}
}
