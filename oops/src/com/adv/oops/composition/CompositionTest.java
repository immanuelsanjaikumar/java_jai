package com.adv.oops.composition;

public class CompositionTest {

	public static void main(String[] args) {
		One oneComp = new One();
		oneComp.m1("Called");
		Two two = oneComp.getTwoObj();
		two.m2("Called");
		oneComp = null;
	}

}
