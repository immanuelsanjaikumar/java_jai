package com.adv.oops.composition.compositiontest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.adv.oops.composition.One;
import com.adv.oops.composition.Two;

public class CompositionTest {
	private One oneComp = new One();
	private Two two = oneComp.getTwoObj();
	
	@Test
	public void compositionTest() {

		String actualResult = oneComp.m1("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
		
	}
	
	@Test
	public void compositionSecTest() {

		String actualResult = two.m2("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
		
	}
	@Test(expected=NullPointerException.class)
	public void compositionSecNullTest() {
		oneComp = null;
		String actualResult = oneComp.m1("Called");
	}
	
	@Test(expected=NullPointerException.class)
	public void compositionSecNullETest() {
		oneComp = null;
		String actualResult = oneComp.getTwoObj().m2("Called");
	}
}
