package com.adv.oops.composition;

public class Two {

	public String m2(String name) {
		System.out.println("Inside m2-Two "+name);
		return name;
	}
}
