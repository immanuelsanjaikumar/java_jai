package com.adv.oops.composition;

public class One {
	private Two twoObj;

	public One() {
		twoObj = new Two();
	}

	public Two getTwoObj() {
		return twoObj;
	}

	public String m1(String name) {
		System.out.println("Inside m1 - One " + name);
		return name;
	}

}
