package com.adv.oops.encapsulation;

import static org.junit.Assert.*;

import org.junit.Test;

public class Encaptest {
	private SuperClass SprCls = new SuperClass();
	private SubClass SubCls = new SubClass();

	@Test
	public void EncapTestPublic() {
		String actualResult = SubCls.Publi("Imman");
		String expectedResult = "Imman";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void EncapTestProtected() {
		String actualResult = SubCls.protectPrivate("Protected");
		String expectedResult = "Protected";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void EncapTestDefault() {
		String actualResult = SprCls.deflt("Default");
		String expectedResult = "Default";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void EncapTestPrivate() {
		String actualResult = SubCls.protectPrivate("Private");
		String expectedResult = "Private";
		assertEquals(expectedResult, actualResult);
	}
}
