package com.adv.oops.encapsulation;

public class SuperClass {
	private String Name;
	int Age;
	protected String City;
	public int Pincode;

	public String Publi(String Name) {
		System.out.println("Public : " + Name);
		return Name;
	}

	protected String Protct(String Name) {
		System.out.println(Name);
		return Name;
	}

	String deflt(String Name) {
		System.out.println(Name);
		return Name;
	}

	private String Prvte(String Name) {
		return Name;
	}

	public String protectPrivate(String Name) {
//		String Private=;
		System.out.println(Prvte(Name));
		return Name;
	}
}
