package com.adv.oops.encapsulation;

public class EncapsulationTest {

	public static void main(String[] args) {

		//----Variable Calling---- 
		
		// Default Calling
		SuperClass sprCls = new SuperClass();
		int age = sprCls.Age;
		sprCls.deflt("Default "+" Age : "+age);
		
		// Protected Calling
		SubClass subCls = new SubClass();
		String city = subCls.City;
		subCls.Protct("Protected");
		subCls.protectPrivate("Private "+" City : "+city);
		
		// Public Calling
		int pincode = subCls.Pincode;
		subCls.Publi("Imman "+" pincode : "+pincode);

	}

}
