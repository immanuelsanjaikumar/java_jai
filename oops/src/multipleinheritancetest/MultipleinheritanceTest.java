package multipleinheritancetest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

//import multipleinheritance.Child;
import multipleinheritance.Parent1;
import multipleinheritance.Parent2;

public class MultipleinheritanceTest {
	// private Child child=new Child();

	@Test
	public void multipleInheritanceP1() {
		String actualResult = Parent1.method1("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void multipleInheritanceP2() {
		String actualResult = Parent2.method1("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
	}
}
