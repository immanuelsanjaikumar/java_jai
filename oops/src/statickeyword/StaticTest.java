package statickeyword;

public class StaticTest {

	public static void main(String[] args) {
		StaticKeyword Static1=new StaticKeyword();
		StaticKeyword Static2=new StaticKeyword();
		StaticKeyword Static3=new StaticKeyword();
		StaticKeyword Static4=new StaticKeyword();
		System.out.println(Static1.name);
		System.out.println(Static2.name);
		System.out.println(Static3.name);
		System.out.println(Static4.name);
		//After Changing in Static Global variable it changes in all the upcoming object referred  
		Static1.name="Immanuel";
		System.out.println("-------------");
		System.out.println(Static1.name);
		System.out.println(Static2.name);
		System.out.println(Static3.name);
		System.out.println(Static4.name);
		System.out.println("------Without Obj We Can Call Static It Is Class Dependent-------");
		System.out.println(StaticKeyword.name);
	}

}
