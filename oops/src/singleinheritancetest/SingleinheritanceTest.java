package singleinheritancetest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import singleinheritance.SubClass;

public class SingleinheritanceTest {
	private SubClass Sub=new SubClass();

	@Test
	public void singleInheritanceChild() {
		String actualResult = Sub.Sub("Child");
		String expectedResult = "Child";
		assertEquals(expectedResult, actualResult);
	}
	@Test
	public void singleInheritanceParent() {
		String actualResult = Sub.Super("Parent");
		String expectedResult = "Parent";
		assertEquals(expectedResult, actualResult);
	}
}
