package singleinheritance;

public class PracticeInheritanceTest {
	public static void main(String[] args) {
		MountainBike singleInheritance=new MountainBike(20, 10, 1);
		System.out.println("Gear : "+singleInheritance.gear);
		System.out.println("Seat Height : "+singleInheritance.seatHeight);
		System.out.println("Bike Speed : "+singleInheritance.speed);
		singleInheritance.applyBrake(1);
		System.out.println("Bike Speed After Brake : "+singleInheritance.speed);
		singleInheritance.speedUp(10);
		System.out.println("Bike Speed After Speed : "+singleInheritance.speed);
	}

}
