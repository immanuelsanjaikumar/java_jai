package singleinheritance;

public class MountainBike extends Bicycle {
	protected int seatHeight;

	public MountainBike(int startHeight, int startSpeed, int startGear) {
		super(startSpeed, startGear);
		this.seatHeight = startHeight;
	}

	public void setHeight(int newValue) {
		seatHeight = newValue;
	}

}
