package multipleinheritance;

public class Child implements Parent1, Parent2 {
//public class Child extends Parent1{
	public String name;

	public void chld() {
		System.out.println("Child");
		Parent1.method1("Called");
		Parent2.method1("Called");
	}
}
