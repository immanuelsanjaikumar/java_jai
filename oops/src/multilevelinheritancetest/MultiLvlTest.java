package multilevelinheritancetest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import multilevelinheritance.ClassD;

public class MultiLvlTest {
	private ClassD multiLvl = new ClassD();

	@Test
	public void multiLvlelinheritance() {
		String actualResult = multiLvl.clsD("Class D Called");
		String expectedResult = "Class D Called";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void multiLvlelinheritance1() {
		String actualResult = multiLvl.clsC("Class C Called");
		String expectedResult = "Class C Called";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void multiLvlelinheritance2() {
		String actualResult = multiLvl.clsB("Class B Called");
		String expectedResult = "Class B Called";
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void multiLvlelinheritance3() {
		String actualResult = multiLvl.clsA("Class A Called");
		String expectedResult = "Class A Called";
		assertEquals(expectedResult, actualResult);
	}
}
