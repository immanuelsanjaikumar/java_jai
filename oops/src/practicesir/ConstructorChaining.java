package practicesir;

public class ConstructorChaining {
	private String Name;
	private int Age;
	private int Rollno;
	private String Address;

	public ConstructorChaining(String Name, int Age, int Rollno) {
		this.Name = Name;
		this.Age = Age;
		this.Rollno = Rollno;
		System.out.println("1st Call");
	}

	public ConstructorChaining(String Name, int Age, int Rollno, String Address) {
		this(Name, Age, Rollno);
		this.Address = Address;
		System.out.println("2nd Call");
	}

	public String getName() {
		return Name;
	}

	public int getAge() {
		return Age;
	}

	public int getRollno() {
		return Rollno;
	}

	public String getAddress() {
		return Address;
	}

}
