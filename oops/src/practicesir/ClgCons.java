package practicesir;

public class ClgCons {
	private int Clgcode;
	private String ClgName;
	private int FoundedYear;
	private String FounderName;
	private String PrincipalName;
	private int NoOfBuses;

//	public ClgCons() {
//		this.Clgcode = Clgcode;
//		this.ClgName = ClgName;
//		this.FounderName = FounderName;
//		this.FoundedYear = FoundedYear;
//		this.PrincipalName = PrincipalName;
//		this.NoOfBuses = NoOfBuses;
//	}

	public ClgCons(int Clgcode) {
		this.Clgcode = Clgcode;
	}

	public ClgCons(String ClgName) {
		this.ClgName = ClgName;
	}

	public ClgCons(int Clgcode, int FoundedYear) {
		this.Clgcode = Clgcode;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String FounderName, String ClgName) {
		this.FounderName = FounderName;
		this.ClgName = ClgName;
	}

	public ClgCons(int Clgcode, String ClgName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
	}

	public ClgCons(String ClgName, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
	}

	public ClgCons(String PrincipalName, String ClgName, String FounderName) {
		this.PrincipalName = PrincipalName;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
	}

	public ClgCons(int Clgcode, int FoundedYear, int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.FoundedYear = FoundedYear;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(int Clgcode, String ClgName, String FounderName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
	}

	public ClgCons(String ClgName, int Clgcode, String FounderName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
	}

	public ClgCons(String ClgName, String FounderName, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
	}

	public ClgCons(int Clgcode, String ClgName, String FounderName, int FoundedYear) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String ClgName, int Clgcode, String FounderName, int FoundedYear) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String ClgName, String FounderName, int Clgcode, int FoundedYear) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String ClgName, String FounderName, String PrincipalName, int FoundedYear) {
		this.PrincipalName = PrincipalName;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String ClgName, String FounderName, int FoundedYear, String PrincipalName) {
		this.PrincipalName = PrincipalName;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(int FoundedYear, String ClgName, String FounderName, String PrincipalName) {
		this.PrincipalName = PrincipalName;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String FounderName, int FoundedYear, String ClgName, String PrincipalName) {
		this.PrincipalName = PrincipalName;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(int Clgcode, int FoundedYear, String ClgName, String FounderName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(int Clgcode, String ClgName, int FoundedYear, String FounderName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(String ClgName, int Clgcode, int FoundedYear, String FounderName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
	}

	public ClgCons(int Clgcode, String ClgName, String FounderName, int FoundedYear, String PrincipalName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(String ClgName, int Clgcode, String FounderName, int FoundedYear, String PrincipalName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(String ClgName, String FounderName, int Clgcode, int FoundedYear, String PrincipalName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(String ClgName, String FounderName, int FoundedYear, String PrincipalName, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(String ClgName, String FounderName, String PrincipalName, int FoundedYear, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(int FoundedYear, int Clgcode, String ClgName, String FounderName, String PrincipalName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(int FoundedYear, String ClgName, int Clgcode, String FounderName, String PrincipalName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(int FoundedYear, String ClgName, String FounderName, String PrincipalName, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(String FounderName, int FoundedYear, String ClgName, String PrincipalName, int Clgcode) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
	}

	public ClgCons(int Clgcode, String ClgName, String FounderName, int FoundedYear, String PrincipalName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String ClgName, int Clgcode, String FounderName, int FoundedYear, String PrincipalName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String ClgName, String FounderName, int Clgcode, int FoundedYear, String PrincipalName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String ClgName, String FounderName, int FoundedYear, String PrincipalName, int Clgcode,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(int FoundedYear, String ClgName, String FounderName, String PrincipalName, int Clgcode,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(int FoundedYear, String PrincipalName, int Clgcode, String ClgName, String FounderName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(int Clgcode, int FoundedYear, String PrincipalName, String ClgName, String FounderName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String ClgName, int Clgcode, int FoundedYear, String PrincipalName, String FounderName,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String PrincipalName, String FounderName, String ClgName, int Clgcode, int FoundedYear,
			int NoOfBuses) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(int Clgcode, int FoundedYear, int NoOfBuses, String PrincipalName, String FounderName,
			String ClgName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public ClgCons(String PrincipalName, String FounderName, int Clgcode, int FoundedYear, int NoOfBuses,
			String ClgName) {
		this.Clgcode = Clgcode;
		this.ClgName = ClgName;
		this.FounderName = FounderName;
		this.FoundedYear = FoundedYear;
		this.PrincipalName = PrincipalName;
		this.NoOfBuses = NoOfBuses;
	}

	public void setClgcode(int Clgcode) {
		this.Clgcode = Clgcode;
	}

	public int getClgcode() {
		return Clgcode;
	}

	public void setClgName(String ClgName) {
		this.ClgName = ClgName;
	}

	public String getClgName() {
		return ClgName;
	}

	public void setFoundeeerName(String FounderName) {
		this.FounderName = FounderName;
	}

	public String getFounderName() {
		return FounderName;
	}

	public void setFoundedYear(int FoundedYear) {
		this.FoundedYear = FoundedYear;
	}

	public int getFoundedYear() {
		return FoundedYear;
	}

	public void setPrincipalName(String PrincipalName) {
		this.PrincipalName = PrincipalName;
	}

	public String getPrincipalName() {
		return PrincipalName;
	}

	public void setNoOfBuses(int NoOfBuses) {
		this.NoOfBuses = NoOfBuses;
	}

	public int getNoOfBuses() {
		return NoOfBuses;
	}
}
