package overloading;

public class OverloadingChild extends OverloadingParent {
	public String method1(String Name) {
		//System.out.println("Method 1 Child");
		System.out.println("Child : "+Name);
		return Name;
	}

	public String method2(String Name) {
		//System.out.println("Method 2 Child");
		System.out.println("Child : "+Name);
		return Name;
	}
}
