package overloading;

public class OverloadingParent {
	public String method1(String Name) {
		//System.out.println("Method 1 Parent");
		System.out.println("Parent : "+Name);
		return Name;
	}
	
	public String method2(String Name,int Age) {
		//System.out.println("Method 2 Parent");
		System.out.println("Parent : "+Name);
		return Name;
	}
}
