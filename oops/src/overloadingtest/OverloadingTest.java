package overloadingtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import overloading.OverloadingChild;

public class OverloadingTest {
	private OverloadingChild Child = new OverloadingChild();

	@Test
	public void PolyOverloading() {
		String actualResult = Child.method1("Called");
		String expectedResult ="Called";
		assertEquals(expectedResult, actualResult);
	}
	@Test
	public void PolyOverloading1() {
		String actualResult = Child.method2("Called",20);
		String expectedResult ="Called";
		assertEquals(expectedResult, actualResult);
	}

}
