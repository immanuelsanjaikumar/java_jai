package hierarchicalinheritance;

public class Child1 extends ClassParent {
	private String city;

	public Child1(String name, int age, String city) {
		super(name, age);
		this.city = city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}
	public String clsChild1(String Name) {
//		System.out.println("Child1 Class Called");
		System.out.println(Name);
		return Name;
	}
}
