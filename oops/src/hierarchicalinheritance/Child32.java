package hierarchicalinheritance;

public class Child32 extends Child3 {
	private long mobileno;

	public Child32(String name, int age, String city, long mobileno) {
		super(name, age, city);
		this.mobileno = mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public long getMobileno() {
		return mobileno;
	}

	public String clsChild32(String Name) {
		//System.out.println("Child32 Class Called");
		System.out.println(Name);
		return Name;
	}
}
