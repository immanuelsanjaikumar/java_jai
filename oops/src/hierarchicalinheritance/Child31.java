package hierarchicalinheritance;

public class Child31 extends Child3 {
	private long mobileno;

	public Child31(String name, int age, String city, long mobileno) {
		super(name, age, city);
		this.mobileno = mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public long getMobileno() {
		return mobileno;
	}

	public String clsChild31(String Name) {
		//System.out.println("Child31 Class Called");
		System.out.println(Name);
		return Name;
	}
}
