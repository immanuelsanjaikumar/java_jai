package hierarchicalinheritance;

public class Child3 extends ClassParent {
	private String city;

	public Child3(String name, int age, String city) {
		super(name, age);
		this.city = city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public String clsChild3(String Name) {
		//System.out.println("Child3 Class Called");
		System.out.println(Name);
		return Name;
	}
}
