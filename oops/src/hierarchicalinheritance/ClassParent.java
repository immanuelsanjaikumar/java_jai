package hierarchicalinheritance;

public class ClassParent {
	private String name;
	private int age;

	public ClassParent(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String clsParent(String Name) {
		//System.out.println("Parent Class Called ");
		System.out.println("Parent : "+Name);
		return Name;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

}
