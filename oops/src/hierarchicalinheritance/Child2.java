package hierarchicalinheritance;

public class Child2 extends ClassParent {
	private String city;

	public Child2(String name, int age, String city) {
		super(name, age);
		this.city = city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public String clsChild2(String Name) {
		//System.out.println("Child2 Class Called");
		System.out.println(Name);
		return Name;
	}
}
