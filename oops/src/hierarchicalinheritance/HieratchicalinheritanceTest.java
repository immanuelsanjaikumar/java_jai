package hierarchicalinheritance;

public class HieratchicalinheritanceTest {

	public static void main(String[] args) {
		System.out.println("Child 1 ");
		Child1 ch1 = new Child1("Imman", 22, "Thanjavur");
		ch1.clsParent("");
		System.out.println("Child 2 ");
		Child2 ch2 = new Child2("Imman", 22, "Thanjavur");
		ch2.clsParent("");
		System.out.println("Child 3 ");
		Child3 ch3 = new Child3("Imman", 22, "Thanjavur");
		ch3.clsParent("");
		System.out.println("Child 31 ");
		Child31 ch31 = new Child31("Imman", 22, "Thanjavur",8778322564l);
		ch31.clsParent("");
		ch31.clsChild3("");
		System.out.println("Child 32 ");
		Child32 ch32 = new Child32("Imman", 22, "Thanjavur",8778322564l);
		ch32.clsParent("");
		ch32.clsChild3("");
	}

}
