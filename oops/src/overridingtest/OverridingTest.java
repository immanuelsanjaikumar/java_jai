package overridingtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import overriding.Child;
import overriding.Parent;

public class OverridingTest {
	private Parent parent = new Child();

	@Test
	public void polyOverRiding() {
		String actualResult = parent.m1("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
	}
}
