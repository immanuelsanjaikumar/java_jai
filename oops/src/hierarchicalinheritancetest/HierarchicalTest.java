package hierarchicalinheritancetest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import hierarchicalinheritance.Child31;

public class HierarchicalTest {
	private Child31 Ch31=new Child31("Imman", 22, "Thanjavur",8754632554l);
	
	@Test
	public void hierarchical() {
		String actualResult = Ch31.clsParent("Called");
		String expectedResult = "Called";
		assertEquals(expectedResult, actualResult);
	}

}
