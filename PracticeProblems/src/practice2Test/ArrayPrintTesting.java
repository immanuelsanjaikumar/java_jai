package practice2Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import practice2.AddConsecutiveArray;
import practice2.ArrayPrint;
import practice2.AscendingArray;
import practice2.DescendingArray;
import practice2.DuplicateArray;
import practice2.EvenArray;
import practice2.MergeArray;
import practice2.MultipleElevel;
import practice2.OddArray;
import practice2.OddEvenIndexArray;
import practice2.PrimeNumber;
import practice2.ShuffleArray;
import practice2.ShuffleArray1;

public class ArrayPrintTesting {
	private ArrayPrint ArrPrint = new ArrayPrint();
	private EvenArray EvenArr = new EvenArray();
	private OddArray OddArr = new OddArray();
	private DuplicateArray Dup = new DuplicateArray();
	private AddConsecutiveArray AddConsArray = new AddConsecutiveArray();
	private PrimeNumber PrimeNo = new PrimeNumber();
	private ShuffleArray shuffleArr = new ShuffleArray();
	private MultipleElevel MulEle = new MultipleElevel();
	private MergeArray MerArr = new MergeArray();
	private ShuffleArray1 ShuffleArr1 = new ShuffleArray1();
	private AscendingArray AscArr = new AscendingArray();
	private DescendingArray DesArr = new DescendingArray();
	private OddEvenIndexArray OddEvenIn = new OddEvenIndexArray();

	@Test
	public void ArrayPrintTest() {
		int actualResult[] = ArrPrint.arrayPrt(1, 10);
		int expectedResult[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		assertArrayEquals(expectedResult, actualResult);
	}

	@Test
	public void EvenArrayTest() {
		int actualResult[] = EvenArr.array1E(21, 27);
		int expectedResult[] = {22,24,26};
		assertArrayEquals(expectedResult, actualResult);
	}

	@Test
	public void OddArrayTest() {
		int actualResult[] = OddArr.OddA(1, 10);
		int expectedResult[] = { 1, 3, 5, 7, 9 };
		assertArrayEquals(expectedResult, actualResult);
	}

	@Test
	public void DuplicateArrayTest() {
		int a[] = { 10, 12, 13, 15, 10, 12, 17, 12, 14, 18, 13, 14, 10, 13 };
		Set<Integer> actualResult = Dup.duplicate(a);
		Set<Integer> expectedResult = new HashSet<>();
		expectedResult.add(10);
		expectedResult.add(12);
		expectedResult.add(13);
		expectedResult.add(14);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void ShuffleArrayTest() {
		int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int actualResult[] = shuffleArr.shuffle(a);
		int expectedResult[] ={1 ,9 ,2 ,8 ,3 ,7 ,4 ,6 ,5 ,5 };
		assertArrayEquals(expectedResult, actualResult);
	}

	@Test
	public void ElevelPrintTest() {
		int actualResult[] = MulEle.mulEleven(151, 255);
		int expectedResult[] = { 154, 165, 176, 187, 198, 209, 220, 231, 242, 253 };
		assertArrayEquals(expectedResult, actualResult);
	}

	

	@Test
	public void MergeArrayTest() {
		int a[] = { 1, 2, 3, 5, 6, 8 };
		int b[] = { 2, 8, 6, 9, 11, 5, 4, 1 };
		Set<Integer> actualResult = MerArr.mergeA(a, b);
		Set<Integer> expectedResult =new HashSet<>();
		expectedResult.add(1);
		expectedResult.add(2);
		expectedResult.add(3);
		expectedResult.add(4);
		expectedResult.add(5);
		expectedResult.add(6);
		expectedResult.add(8);
		expectedResult.add(9);
		expectedResult.add(11);
		assertEquals(expectedResult, actualResult);
	}
	

	@Test
	public void shuffle1ArrayTest() {
		int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int actualResult[] = ShuffleArr1.shuffle1(a);
		int expectedResult[] ={1 ,3 ,5 ,7 ,9 ,10 ,8 ,10 ,7 ,3 };
		assertArrayEquals(expectedResult, actualResult);
	}
	

	@Test
	public void AscArrayTest() {
		int arr[] = { 1, 9, 5, 2, 3, 4, 7, 6 };
		List<Integer> actualResult = AscArr.AscArray(arr);
		List<Integer> expectedResult =new ArrayList<>();
		expectedResult.add(1);
		expectedResult.add(2);
		expectedResult.add(3);
		expectedResult.add(4);
		expectedResult.add(5);
		expectedResult.add(6);
		expectedResult.add(7);
		expectedResult.add(9);
		assertEquals(expectedResult, actualResult);
	}
	

	@Test
	public void DescArrayTest() {
		int arr[] = { 1, 9, 5, 2, 3, 4, 7, 6 };
		List<Integer> actualResult = DesArr.DesArr(arr);
		List<Integer> expectedResult =new ArrayList<>();
		expectedResult.add(9);
		expectedResult.add(7);
		expectedResult.add(6);
		expectedResult.add(5);
		expectedResult.add(4);
		expectedResult.add(3);
		expectedResult.add(2);
		expectedResult.add(1);
		assertEquals(expectedResult, actualResult);
	}
	

	@Test
	public void OddEvenArrayTest() {
		int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int actualResult = OddEvenIn.Index(a);
		int expectedResult = -5;
		assertEquals(expectedResult, actualResult);
	}
	

	@Test
	public void PrimeArrayTest() {
		int actualResult[] = PrimeNo.primeNum(1, 10);
		int expectedResult[] ={2 ,3 ,5 ,7 };
		assertArrayEquals(expectedResult, actualResult);
	}
	

	@Test
	public void AddConsecutiveArrayTest() {
		int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		Set<Integer> actualResult = AddConsArray.AddConsArray(a);
		Set<Integer> expectedResult =new TreeSet<>();
		expectedResult.add(3);
		expectedResult.add(5);
		expectedResult.add(7);
		expectedResult.add(9);
		expectedResult.add(11);
		expectedResult.add(13);
		expectedResult.add(15);
		expectedResult.add(17);
		assertEquals(expectedResult, actualResult);
	}
	
}
