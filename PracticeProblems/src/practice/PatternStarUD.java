package practice;

public class PatternStarUD {
	public int PSUD(int n) {
		for (int i = 0; i <= n; i++) {

			for (int k = 1; k <= i + 1; k++) {
				System.out.print(" ");
			}

			for (int j = n; j >= i; j--) {
				System.out.print("*" + " ");
			}
			System.out.print("\n");
		}
		for (int i = 1; i <= n; i++) {

			for (int k = i; k <= n; k++) {
				System.out.print(" ");
			}

			for (int j = 1; j <= i + 1; j++) {
				System.out.print("*" + " ");
			}
			System.out.print("\n");
		}
		return 0;
	}
}
