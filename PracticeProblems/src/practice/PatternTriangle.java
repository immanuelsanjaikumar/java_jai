package practice;

public class PatternTriangle {
	public int PT() {
		for (int i = 1; i <=4; i++) {
			for (int j = 4; j >= i; j--) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*"+" ");
			}
			System.out.println("\n");
		}
		return 0;
	}
}
