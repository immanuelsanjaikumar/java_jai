package practice;

import java.util.Scanner;

public class Choice {
	public int ch() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose To Display" + "\n" + "1.Number From 1-100" + "\n" + "2.Reverse Number From 1-100"
				+ "\n" + "3.Odd Number From 1-100" + "\n" + "4.Even From 1-100" + "\n" + "5.Odd Number in Given Input"
				+ "\n" + "6.Even Number in Given Input" + "\n" + "7.Table's for Given Number" + "\n" + "8.Print 1 12"
				+ "\n" + "9.Print 1 22" + "\n" + "10.Print Star * ** *** ****" + "\n" + "11.Print Star **** *** ** *"
				+ "\n" + "12.Print Star * ** *** **** *** ** *" + "\n" + "13.Print Traingle" + "\n"
				+ "14.Print Upside Down Triangle" + "\n" + "15.Print Diamond"+ "\n" + "16.Print Diamond Outer Star"
				+ "\n" + "17.Print Diamond Star Upside Down ... *** ** * ** *** ");
		int choose = sc.nextInt();
		switch (choose) {
		case 1: {
			Array1_100 A1_100 = new Array1_100();
			A1_100.array();
			break;
		}
		case 2: {
			Reverse1_100 R1_100 = new Reverse1_100();
			R1_100.reverse();
			break;
		}
		case 3: {
			OddNumber1_100 Odd1_100 = new OddNumber1_100();
			Odd1_100.Odd();
			break;
		}
		case 4: {
			EvenNumber1_100 Even1_100 = new EvenNumber1_100();
			Even1_100.Even();
			break;
		}
		case 5: {
			System.out.println("From Value : ");
			int a = sc.nextInt();
			System.out.println("To Value : ");
			int b = sc.nextInt();
			OddNumberGivenInput Ogi = new OddNumberGivenInput();
			Ogi.OddGI(a,b);
			break;
		}
		case 6: {
			System.out.println("From Value : ");
			int a = sc.nextInt();
			System.out.println("To Value : ");
			int b = sc.nextInt();
			EvenNumberGivenInput Egi = new EvenNumberGivenInput();
			Egi.EvenGI(a,b);
			break;
		}
		case 7: {
			System.out.println("Which Table : ");
			int a = sc.nextInt();
			System.out.println("Upto Which Value: ");
			int b = sc.nextInt();
			TablesMultiplication TablesMul = new TablesMultiplication();
			TablesMul.Tables(a,b);
			break;
		}
		case 8: {
			System.out.println("Input : ");
			int n = sc.nextInt();
			PatternPrint_1_12_ P1_12 = new PatternPrint_1_12_();
			P1_12.PatternPrint1_12(n);
			break;
		}
		case 9: {
			PatternPrint1_22 P1_22 = new PatternPrint1_22();
			P1_22.Patternnum1_22();
			break;
		}
		case 10: {
			PatternStart1 PS1 = new PatternStart1();
			PS1.PS();
			break;
		}
		case 11: {
			PatternStarUpsideDown PSUD1 = new PatternStarUpsideDown();
			PSUD1.PSUD();
			break;
		}
		case 12: {
			PatternStarLeftRight PSLR1 = new PatternStarLeftRight();
			PSLR1.PSLR();
			break;
		}
		case 13: {
			PatternTriangle PT1 = new PatternTriangle();
			PT1.PT();
			break;
		}
		case 14: {
			PatternTriangleReverse PTR1 = new PatternTriangleReverse();
			PTR1.PTR();
			break;
		}
		case 15: {
			PatternDiamond Diamond1 = new PatternDiamond();
			Diamond1.Diamond();
			break;
		}
		case 16: {
			System.out.println("Input : ");
			int n=sc.nextInt();
			DiamondOuterStar DOS1 = new DiamondOuterStar();
			DOS1.DOS(n);
			break;
		}
		case 17: {
			System.out.println("Input : ");
			int n=sc.nextInt();
			PatternStarUD PSUD1 = new PatternStarUD();
			PSUD1.PSUD(n);
			break;
		}
		default: {
			System.out.println("Invalid Choice");
			break;
		}
		}
		System.out.println("Whether You Want To Display Another? Yes(1)/No(Any Key Otherthan 1)");
		int d=sc.nextInt();
		if (d == 1) {
			ch();
		} else {
			System.out.println("Exit");
		}
		sc.close();
		return 0;
	}

}
