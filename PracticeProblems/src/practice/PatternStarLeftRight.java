package practice;

public class PatternStarLeftRight {
	public int PSLR() {
		for (int i = 1; i <=4; i++) {
			for(int j=1;j<=i;j++) {
				System.out.print("*"+" ");
				}
			System.out.println("\n");
		}
		for (int i = 1; i <= 3; i++) {
			for (int j = 3; j >= i; j--) {
				System.out.print("*" + " ");
			}
			System.out.println("\n");
		}
		return 0;
	}
}
