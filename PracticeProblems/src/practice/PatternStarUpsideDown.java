package practice;

public class PatternStarUpsideDown {
	public int PSUD() {
		for (int i = 1; i <= 4; i++) {
			for (int j = 4; j >= i; j--) {
				System.out.print("*" + " ");
			}
			System.out.println("\n");
		}
		return 0;
	}
}
