package practice;

public class PatternDiamond {
	public int Diamond() {
		for (int i = 1; i <= 4; i++) {
			for (int j = 4; j >= i; j--) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*" + " ");
			}
			System.out.println("\n");
		}

		for (int i = 1; i <= 3; i++) {

			for (int k = 1; k <= i+1; k++) {
				System.out.print(" ");
			}

			for (int j = 3; j >= i; j--) {
				System.out.print("*" + " ");
			}

			System.out.println("\n");
		}
		return 0;
	}
}
