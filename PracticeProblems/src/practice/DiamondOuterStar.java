package practice;
public class DiamondOuterStar {
	public int DOS(int n) {
			for (int i = 1; i <= n; i++) {
				for (int j = n; j >= i; j--) {
					System.out.print("*");
				}
				for (int k = 1; k <= i; k++) {
					System.out.print("  ");
				}
				for (int m = n; m >= i; m--) {
					System.out.print("*");
				}
				System.out.print("\n");
			}

			for (int i = 1; i <=n-1; i++) {

				for (int k = 1; k <= i+1; k++) {
					System.out.print("*");
				}

				for (int j = n-1; j >= i; j--) {
					System.out.print("  ");
				}
				
				for (int k = 1; k <= i+1; k++) {
					System.out.print("*");
				}
				System.out.print("\n");
			}
			return 0;
		}
}