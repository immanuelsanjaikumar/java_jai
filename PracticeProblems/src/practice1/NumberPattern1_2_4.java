package practice1;


public class NumberPattern1_2_4 {
	public int tablesPattern(int n) {
		if (n >= 0) {
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= i; j++) {
					System.out.print(i * j + " ");
				}
				System.out.println("\n");
			}
		} else {
			System.out.println("Invalid Input");
		}
		return 0;
	}

}
