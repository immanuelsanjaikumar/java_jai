package practice1;

import java.util.*;

public class IfClass {
	public static void main(String a[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input : ");
		int age = sc.nextInt();
		sc.close();
		if (age <= 5 && age >= 1) {
			System.out.println("Kid");
		} else if (age > 5 && age <= 18) {
			System.out.println("School Student");
		} else if (age > 18 && age <= 23) {
			System.out.println("College Student");
		} else if (age >= 25 && age <= 40) {
			System.out.println("Earner");
		} else if (age <65 && age > 40) {
			System.out.println("Elder");
		} else if (age > 65 && age < 80) {
			System.out.println("Person is Senior Citizen");
		} else if (age >= 80) {
			System.out.println("Very Old");
		} else {
			System.out.println("Invalid Input");
		}

	}
}
