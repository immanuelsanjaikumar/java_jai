package ArraySir;

public class DuplicateArray {
	public static void main(String ar[]) {
		int a[] = { 10, 12, 13, 15, 10, 12, 17, 12, 14, 18, 13, 14, 10, 13 };
		int noDupArry[] = new int[a.length];// 13
		int noCount[] = new int[a.length];// 13
		int previousIndex = 0;
		int currentNo = 0;

		for (int i = 0; i < a.length; i++) { // (0;0<15;0++)->true
			currentNo = a[i];//10
			boolean isPresent = false;
			int currentIndex = 0;//0
			for (int j = 0; j < noDupArry.length; j++) { //0
				if (currentNo == noDupArry[j]) {
					isPresent = true;
					currentIndex = j;
					break;
				}
			}
			if (isPresent) {
				noCount[currentIndex] = noCount[currentIndex] + 1;
			} else {
				noDupArry[previousIndex] = currentNo;
				noCount[previousIndex] = noCount[previousIndex] + 1;
				previousIndex++;
			}
		}
		for (int i = 0; i < noCount.length; i++) { // (0;)
			if (noCount[i] > 1) {
				System.out.println(noDupArry[i]);
			}
		}
	}
}
