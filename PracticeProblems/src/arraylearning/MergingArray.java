package arraylearning;

public class MergingArray {
	public static void main(String ar[]) {
		int[] a= {1,2,3,4};
		int[] b= {5,6};
		int len=a.length+b.length;
		int[] c=new int[len];
		for(int i=0;i<a.length;i++) {
			c[i]=a[i];
		}
		int j=0;
		for(int i=a.length;i<c.length;i++) {//a.length=4 c.length=7
			c[i]=b[j++];
		}
		for(int i:c) {
			System.out.print(i+" ");
		}
	}

}
