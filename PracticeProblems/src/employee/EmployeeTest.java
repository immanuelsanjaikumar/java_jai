package employee;

public class EmployeeTest {
	public static void main(String[] args) {
		
		// Method 1 - Directly Assigning Values Individually
		
		/*EmployeeDataType empData = new EmployeeDataType();
		empData.name = "Imman";
		empData.age = 22;
		empData.email = "Imman@gmail.com";
		empData.height = 162;

		EmployeeDataType empData1 = new EmployeeDataType();
		empData1.name = "David";
		empData1.age = 21;
		empData1.email = "David@gmail.com";
		empData1.height = 164;

		EmployeeDataType empData2 = new EmployeeDataType();
		empData2.name = "Raina";
		empData2.age = 18;
		empData2.email = "raina@gmail.com";
		empData2.height = 160;

		Employee array1[] = { empData, empData1, empData2 };
		for (int i = 0; i < array1.length; i++) {
			System.out.println(array1[i].name);
		}

		
		// Method 2 -  Creating Separate Class for Input,Output for non Repetition
		
		//Input
		EmpInput EInput=new EmpInput();
		EmployeeDataType Emp1=EInput.input("Imman",22, "Imman@gmail.com",160);
		EmployeeDataType Emp2=EInput.input("David",21, "David@gmail.com",162);
		
		//Output
		EmpOutput Eoutput=new EmpOutput();
		Eoutput.empPrint(Emp1);
		Eoutput.empPrint(Emp2);*/
	
		// Method 3 - Getter & Setter
		GetterSetter G=new GetterSetter();
		
		//Setter
		String name1="Imman";
		G.setName(name1);
		int age1=22;
		G.setAge(age1);
		String email1="Imman@gmail.com";
		G.setEmail(email1);
		int height1=160;
		G.setHeight(height1);
		//Getter
		String name11=G.getName();
		System.out.println("Name : "+name11);
		int age11=G.getAge();
		System.out.println("Age : "+age11);
		String email11=G.getEmail();
		System.out.println("Email : "+email11);
		int height11=G.getHeight();
		System.out.println("Height : "+height11);
	}
}
