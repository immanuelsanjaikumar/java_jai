package practice2_1;

import practice2.Print;

//merge two arrays and remove duplicates,int a[]={1,2,3,5,6,8} 
//int b[]={2,8,6,9,11,5,4,1} 
public class DupMergeArr {
	public int[] DupMerArr(int[] a, int[] b) {
		int[] m = new int[a.length + b.length];
		int count = 0;
		for (int i : a) {
			m[count] = i;
			count++;
		}
		for (int i : b) {
			m[count] = i;
			count++;
		}
		int mlen = m.length;
		for (int i = 0; i < mlen - 1; i++) {
			for (int j = i + 1; j < mlen; j++) {
				if (m[i] == m[j]) {
					m[j] = m[mlen - 1];
					mlen--;
				}
			}
		}
		int[] x = new int[mlen];
		for (int k = 0; k < mlen; k++) {
			x[k] = m[k];
//			System.out.println(x[k] + " ");
		}
		Print prt = new Print();
		prt.print1(x);
		return x;
	}
}
