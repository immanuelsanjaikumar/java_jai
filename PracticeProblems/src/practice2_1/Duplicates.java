package practice2_1;

import practice2.Print;
import practice2.RemovingZeros;

public class Duplicates {// 10 12 13 14
	public int[] dup(int[] c) {

		int frequency[] = new int[c.length];
		for (int i = 0; i < c.length; i++) {// i=0
			int CurrentNo = c[i];// 10
			int count = 1;// 1
			for (int j = i + 1; j < c.length; j++) {// 1<14 2<14 3<14 (True)4<14
				{
					if (CurrentNo == c[j]) {// 10==12 10==13 10==15 True(10==10)
						count++;// True(count=2)
						frequency[j] = -1;// (True)f[4]=-1
					}
				}
				if (frequency[i] != -1) {// (true)(f[0]->(0))!=-1 (true)(f[1]->(0))!=-1 (true)(f[2]->(0))!=-1
					frequency[i] = count;// f[0]=1 f[1]=1 f[2]=1
				}
			}
		}

//		for (int i = 0; i < frequency.length; i++) {
//			if (frequency[i] > 1) {
//				System.out.print(c[i] + " ");
//			}
//		}
//		int d = 0;
//		for (int i = 0; i < frequency.length; i++) {
//			if (frequency[i] > 1) {
//				d++;
//			}
//		}

		int[] x = new int[c.length];
		for (int i = 0; i < frequency.length; i++) {
			if (frequency[i] > 1) {
				x[i] = c[i];
				//System.out.println(c[i]);
			}
		}
		RemovingZeros RemZero = new RemovingZeros();
		int[] arr = RemZero.Zero(x);
		Print prt = new Print();
		prt.print1(arr);
		//System.out.println("Value : "+d);
		return arr;
	}
}
