package practice2_1Test;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import practice2_1.DupMergeArr;
import practice2_1.Duplicates;

public class Practice2_1JunitTest {
	private DupMergeArr DupMergA = new DupMergeArr();
	private Duplicates DuplicatesArr = new Duplicates();

	@Test
	public void dupMergAr() {
		int a[] = { 1, 2, 3, 5, 6, 8 };
		int b[] = { 2, 8, 6, 9, 11, 5, 4, 1 };
		int[] actualResult = DupMergA.DupMerArr(a, b);
		int[] expectedResult = { 1, 2, 3, 5, 6, 8, 4, 9, 11 };
		assertArrayEquals(expectedResult, actualResult);
	}

	@Test
	public void DuplicatesA() {
		int c[] = { 10, 12, 13, 15, 10, 12, 17, 12, 14, 18, 13, 14, 10, 13 };
		int[] actualResult = DuplicatesArr.dup(c);
		int[] expectedResult = {10 ,12 ,13 ,14 };
		assertArrayEquals(expectedResult, actualResult);
	}
}
