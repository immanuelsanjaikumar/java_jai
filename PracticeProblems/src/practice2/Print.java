package practice2;

import java.util.*;

public class Print {
	public int[] print1(int[] arr) {
		for (int i : arr) {
			System.out.print(i + " ");
		}
		System.out.println(" ");
		return arr;
	}

	public static void print2(Set<Integer> hashSet) {
		System.out.print(hashSet);
		System.out.println(" ");
	}

	public static void print3(List<Integer> Sortedlist) {
		System.out.print(Sortedlist);
		System.out.println(" ");
	}

	public static void print5() {
		System.out.print("Invalid Entry");
		System.out.println(" ");
	}

	public static void print6(int o, int e, int k) {
		System.out.println("\n" + "Odd : " + o);
		System.out.println("Even : " + e);
		System.out.println("Subtract : " + k);
		System.out.println(" ");
	}

}
