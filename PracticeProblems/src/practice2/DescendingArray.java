package practice2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
//import java.util.Scanner;
import java.util.stream.Collectors;

public class DescendingArray {
	public List<Integer> DesArr(int[] arr) {
		//int arr[] = { 1, 9, 5, 2, 3, 4, 7, 6 };
		/*Scanner tr=new Scanner(System.in);
		int a[] = new int[ElementSize];
		for(int p=0;p<a.length;p++) {
			int k=tr.nextInt();
			a[p]=k;
		}*/
//		for (int i = 0; i < arr.length; i++) {
//			for (int j = i + 1; j < arr.length; j++) {
//				int temp = 0;
//				if (arr[i] < arr[j]) {
//					temp = arr[i];
//					arr[i] = arr[j];
//					arr[j] = temp;
//				}
//
//			}
//			System.out.print(arr[i]+" ");
//		}
		List<Integer> arraylist = new ArrayList<>();
		for (int i : arr) {
			arraylist.add(i);
		}
		List<Integer> Sortedlist = arraylist.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		Print.print3(Sortedlist);
		return Sortedlist;
	}
}
