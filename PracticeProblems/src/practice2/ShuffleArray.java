package practice2;

//import java.util.Scanner;

/*int a[]={1,2,3,4,5,6,7,8,9,10} 
shuffle the given array to get b[]={1,9,2,8,3,7,4,5,6}*/
public class ShuffleArray {
	public int[] shuffle(int[] a) {
		/*
		 * Scanner tr=new Scanner(System.in); int a[] = new int[ElementSize]; for(int
		 * p=0;p<a.length;p++) { int k=tr.nextInt(); a[p]=k; }
		 */
		// int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
		int c, k = 0;
		c = a.length - 2;
		int[] b = new int[a.length];
		for (int i = 0; i < (a.length / 2); i++, c--) {
			if (i % 2 == 0) {
				b[k] = a[i];
//				System.out.println(k+" : Index "+" Value : "+ b[k]);
				k++;
				b[k] = a[c];
//				System.out.println(k+" : Index "+" Value : "+ a[c]);
				k++;
			} else {
				b[k] = a[i];
//				System.out.println(k+" : Index "+" Value : "+ b[k]);
				k++;
				b[k] = a[c];
//				System.out.println(k+" : Index "+" Value : "+ a[c]);
				k++;
			}
		}
		Print prt = new Print();
		prt.print1(b);
		return b;
	}
}
