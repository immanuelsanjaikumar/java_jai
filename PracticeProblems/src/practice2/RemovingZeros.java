package practice2;

public class RemovingZeros {
	public int[] Zero(int[] array1) {
		int d=0;
		for (int i = 0; i < array1.length; i++) {
			if (array1[i] != 0)
				d++;
		}
		int[] newArray = new int[d];
		for (int i = 0, j = 0; i < array1.length; i++) {
			if (array1[i] != 0) {
				newArray[j] = array1[i];
				j++;
			}
		}
		return newArray;
	}
}
