package practice2;

import java.util.Scanner;

public class Choice {

	public int ch() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose To Display" + "\n" + "1.Print 1 to 100 and store in an array " + "\n"
				+ "2.Print even number bet 142 to 239 and store in an array " + "\n"
				+ "3.Print odd number bet 142 to 239 and store in an array " + "\n"
				+ "4.Int a[]={10,12,13,15,10,12,17,12,14,18,13,14,10,13}.Store duplicates number in an array without repeatation."
				+ "\n" + "5.Int a[]={1,2,3,4,5,6,7,8,9,10},Shuffle the given array to get b[]={1,9,2,8,3,7,4,5,6} "
				+ "\n" + "6.Print multiples of eleven bet 151 to 255 and store in an array " + "\n"
				+ "7.Merge two arrays and remove duplicates,int a[]={1,2,3,5,6,8} int b[]={2,8,6,9,11,5,4,1} " + "\n"
				+ "8.int a[]={1,2,3,4,5,6,7,8,9,10},Shuffle the given array to get b[]={1,3,5,7,9,10,8,6,4,2} " + "\n"
				+ "9.Sort the given array a[]={1,9,5,2,3,4,7,6} in ascending" + "\n"
				+ "10.Sort the given array a[]={1,9,5,2,3,4,7,6} in descending" + "\n"
				+ "11.Int a[]={1,2,3,4,5,6,7,8,9,10},add odd index values and add even index value and subtract both."
				+ "\n" + "12.Print prime number from 1 to 100 and store in an array." + "\n"
				+ "13.Add consecutive in ddex value a={1,2,3,4,5,6,7,8,9} to ger b={3,5,7,9,11,13,15,17} " + "\n");
		System.out.print("Choose : ");
		int choose = sc.nextInt();
		switch (choose) {
		case 1: {
			int start = sc.nextInt();
			int end = sc.nextInt();
			ArrayPrint arrPrint = new ArrayPrint();
			arrPrint.arrayPrt(start, end);
			break;
		}
		case 2: {
			int start = sc.nextInt();
			int end = sc.nextInt();
			EvenArray EvenArr = new EvenArray();
			EvenArr.array1E(start, end);
			break;
		}
		case 3: {
			int start = sc.nextInt();
			int end = sc.nextInt();
			OddArray OddArr = new OddArray();
			OddArr.OddA(start, end);
			break;
		}
		case 4: {
			// int ElementSize=sc.nextInt();
			DuplicateArray Dup = new DuplicateArray();
			int a[] = { 10, 12, 13, 15, 10, 12, 17, 12, 14, 18, 13, 14, 10, 13 };
			Dup.duplicate(a);
			break;
		}
		case 5: {
			// int ElementSize=sc.nextInt();
			ShuffleArray shuffleArr = new ShuffleArray();
			int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			shuffleArr.shuffle(a);
			break;
		}
		case 6: {
			/*
			 * int start = sc.nextInt(); int end = sc.nextInt();
			 */
			int start = 151;
			int end = 255;
			MultipleElevel MulEle = new MultipleElevel();
			MulEle.mulEleven(start, end);
			break;
		}
		case 7: {
			// System.out.println("Size of Value 1 : ");
			// int ElementSize1=sc.nextInt();
			int a[] = { 1, 2, 3, 5, 6, 8 };
			int b[] = { 2, 8, 6, 9, 11, 5, 4, 1 };
			// System.out.println("Size of Value 2 : ");
			// int ElementSize2=sc.nextInt();
			MergeArray MerArr = new MergeArray();
			// MerArr.mergeA(ElementSize1,ElementSize2);
			MerArr.mergeA(a, b);
			break;
		}
		case 8: {
			// int ElementSize=sc.nextInt();
			int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			ShuffleArray1 ShuffleArr1 = new ShuffleArray1();
			ShuffleArr1.shuffle1(a);
			break;
		}
		case 9: {
			//int ElementSize = sc.nextInt();
			int arr[] = { 1, 9, 5, 2, 3, 4, 7, 6 };
			AscendingArray AscArr = new AscendingArray();
			AscArr.AscArray(arr);
			break;
		}
		case 10: {
			//int ElementSize = sc.nextInt();
			int arr[] = { 1, 9, 5, 2, 3, 4, 7, 6 };
			DescendingArray DesArr = new DescendingArray();
			DesArr.DesArr(arr);
			break;
		}
		case 11: {
			/*int ElementSize = sc.nextInt();*/
			int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			OddEvenIndexArray OddEvenIn = new OddEvenIndexArray(); //
			OddEvenIn.Index(a);
			break;
		}
		case 12: {
			int start = sc.nextInt();
			int end = sc.nextInt();
			PrimeNumber PrimeNo = new PrimeNumber();
			PrimeNo.primeNum(start, end);
			break;
		}
		case 13: {
			//int ElementSize = sc.nextInt();
			int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			AddConsecutiveArray AddConsArray = new AddConsecutiveArray();
			AddConsArray.AddConsArray(a);
			break;
		}
		default: {
			System.out.println("Invalid Choice");
			break;
		}
		}
		System.out.println("\n" + "Whether You Want To Display Another? Yes(1)/No(Any Key Otherthan 1)");
		int d = sc.nextInt();
		if (d == 1) {
			ch();
		} else {
			System.out.println("Exit");
		}
		sc.close();
		return 0;
	}
}
