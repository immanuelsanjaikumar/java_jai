package practice2;

public class OddArray {
	public int[] OddA(int start, int end) {
		int[] array1 = new int[end + 1];
		int k = 0;
		for (int i = start; i <= end; i++) {
			if (i % 2 != 0) {
				array1[k] = i;
				k++;
			}
		}
		RemovingZeros RemZero=new RemovingZeros();
		int[] arr= RemZero.Zero(array1);
		Print prt=new Print();
		prt.print1(arr);
		return arr;
	}
}
