package practice2;

public class MultipleElevel {
	public int[] mulEleven(int start, int end) {
		int arr[] = new int[(end - start) + 1];
		int k = 0;
		for (int i = start; i <= end; i++) {
			if (i % 11 == 0) {
				arr[k++] = i;
			}
		}
		RemovingZeros RemZero = new RemovingZeros();
		int[] arr1 = RemZero.Zero(arr);
		Print prt = new Print();
		prt.print1(arr1);
		return arr1;
	}
}
