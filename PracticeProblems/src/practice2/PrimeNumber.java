package practice2;

//import java.util.*;

public class PrimeNumber {
	public int[] primeNum(int start, int end) {
		// Set<Integer> hashset = new HashSet<>();
		int k = 0;
		int a[] = new int[(end - start) + 2];
		for (int i = start; i <= end; i++) {
			int n = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					n++;
				}
			}
			if (n == 2) {
				// hashset.add(i);
				a[k] = i;
				k++;
				// System.out.print(hashset);
//				for (int l : hashset) {
//					System.out.print(l + " ");
//				}
			}
		}
		RemovingZeros RemZero = new RemovingZeros();
		int[] arr = RemZero.Zero(a);
		Print prt = new Print();
		prt.print1(arr);
		System.out.println("\n" + arr.length);
		return arr;
	}
}
