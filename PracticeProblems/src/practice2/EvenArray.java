package practice2;

public class EvenArray {
	public int[] array1E(int start, int end) {
		int[] array1 = new int[(end - start)+ 2];
		int k = 0;
		if (start < end) {
			for (int i = start; i <= end; i++) {
				if (i % 2 == 0) {
					array1[k] = i;
					// System.out.print(array1[k]+" ");
					k++;
				}
			}
		} else {
			System.out.println("Invalid Entry");
		}
		RemovingZeros RemZero = new RemovingZeros();
		int[] arr = RemZero.Zero(array1);
		Print prt = new Print();
		prt.print1(arr);
		return arr;
	}
}
