package practice2;

import java.util.*;

public class DuplicateArray {
	public Set<Integer> duplicate(int[] a) {
		//Scanner tr = new Scanner(System.in);
		//int a[] = new int[ElementSize];
		int c = a.length;
		/*for (int p = 0; p < c; p++) {
			int k = tr.nextInt();
			a[p] = k;
		}*/
		Set<Integer> hashSet = new HashSet<>();
		for (int i = 0; i < c; i++) {
			for (int j = i + 1; j < c; j++) {
				if (a[i] == a[j])
					hashSet.add(a[j]);
			}
		}
		Print.print2(hashSet);
		return hashSet;
	}
}
