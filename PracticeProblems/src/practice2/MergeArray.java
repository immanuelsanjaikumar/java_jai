package practice2;

/*merge two arrays and remove duplicates 
int a[]={1,2,3,5,6,8} int b[]={2,8,6,9,11,5,4,1} */
import java.util.*;

public class MergeArray {
	//public Set<Integer> mergeA(int ElementSize1,int ElementSize2) {
	public Set<Integer> mergeA(int[] a,int[] b) {
		//int a[] = { 1, 2, 3, 5, 6, 8 };
		/*Scanner tr=new Scanner(System.in);
		System.out.println("Enter Values for 1st : ");
		int a[] = new int[ElementSize1];
		for(int p=0;p<a.length;p++) {
			int k=tr.nextInt();
			a[p]=k;
		}
		System.out.println("Enter Values for 2nd : ");
		int b[] = new int[ElementSize2];
		for(int p=0;p<b.length;p++) {
			int k=tr.nextInt();
			b[p]=k;
		}*/
		//int b[] = { 2, 8, 6, 9, 11, 5, 4, 1 };

		Set<Integer> hashset = new HashSet<>();
		for (int i : a) {
			hashset.add(i);

		}
		for (int i : b) {
			hashset.add(i);

		}
		Print.print2(hashset);
		return hashset;
	}
}
