package practice3;

public class Student {
	private String name;
	private int age;
	private String email;
	private String city;

//	public Student() {
//		this.name = name;
//		this.age = age;
//		this.email = email;
//		this.city = city;
//	}

	public Student(String name, int age, String email, String city) {
		this.name = name;
		this.age = age;
		this.email = email;
		this.city = city;
	}

	public Student(String name, int age, String email) {
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public Student(int age, String email) {
		this.email = email;
		this.age = age;
	}

	public Student(String name) {
		this.name = name;
	}

	public Student(int age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getEmail() {
		return email;
	}

	public String getCity() {
		return city;
	}
}
