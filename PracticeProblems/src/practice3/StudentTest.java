package practice3;

public class StudentTest {
	public static void main(String ar[]) {
		// Student std=new Student();
		/*
		 * std.setName("Imman"); 
		 * System.out.println(std.getName());
		 */

//		Student stdSmp = new Student();
//		System.out.println(stdSmp.getName());
//		Student stdSmp1 = new Student();
//		System.out.println(stdSmp1.getName());
		
		//Student stdDefault = new Student();
		/*
		 * stdDefault.setName("Imman"); stdDefault.setAge(22);
		 * stdDefault.setEmail("Imman@gmail.com"); 
		 * stdDefault.setCity("Thanjavur");
		 */
		//System.out.println(stdDefault.getName() + " " + stdDefault.getCity());

		Student stdArguments = new Student("Imman",22,"Imman@gmail.com","Chennai");
		System.out.println(stdArguments.getName() + " " + stdArguments.getAge()+ " " + stdArguments.getEmail()+ " " + stdArguments.getCity());
		
		Student stdArguments1 = new Student("Imman",22,"Imman@gmail.com");
		System.out.println(stdArguments1.getName() + " " + stdArguments1.getAge()+ " " + stdArguments1.getEmail());
		
		Student stdArguments2 = new Student("Imman",22);
		System.out.println(stdArguments2.getName() + " " + stdArguments2.getAge());
		
		Student stdArguments3 = new Student("Imman");
		System.out.println(stdArguments3.getName());
		
		Student stdArguments4 = new Student(22);
		System.out.println(stdArguments4.getAge());
		

		Student stdArguments5 = new Student("jj@gmail.com");
		System.out.println(stdArguments5.getEmail());
		

		Student stdArguments6 = new Student("Thanjavur");
		System.out.println(stdArguments6.getCity());
	}

}
