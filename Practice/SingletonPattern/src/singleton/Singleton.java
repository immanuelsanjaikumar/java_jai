package singleton;

public class Singleton {

	static Singleton objRef = null;

	private Singleton() {
	}

	public static Singleton getSingleton() {
		if (objRef == null)
			objRef = new Singleton();
		return objRef;
	}
}
