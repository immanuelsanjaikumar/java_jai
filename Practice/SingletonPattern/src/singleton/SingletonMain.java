package singleton;


public class SingletonMain {

	public static void main(String[] args) {
		
		Singleton singleton=Singleton.getSingleton();
		System.out.println("Object 1 : "+singleton);
		
		Singleton singleton2=Singleton.getSingleton();
		System.out.println("Object 2 : "+singleton2);
	}

}
