package com.tatasteel.prac;

//input1: 6 
//input2: {49,5,3,2,10}
//Output: {0,0,1,3,4,0}

//Explanation:
//The first student has no one ahead of him her. So the answer is O 
//The second student has PSC greater than the first So the answer is 0
//Third student 9 is greater than 5. So the answer is 1.
//Fourth student 9, 4 and 5 are greater than 3 So the answer a 3
//Fifth student 3, 5, 9 and 4 are greater than 2 So the answer is 4
//Sixth student: No one ahead has a greater PSC ,so the answer is 0

public class TataSteelQ1 {

	public static void main(String[] args) {
		// int input = 6;
		int input2[] = { 4, 9, 5, 3, 2, 10,15};

		TataSteelQ1.tSP1(input2);

	}

	public static void tSP1(int input2[]) {
		int output[] = new int[input2.length];
		for (int i = 0; i < input2.length; i++) {
			int count = 0;
			if (i != 0) {
				for (int j = 0; j < i; j++) {
					if (input2[i] < input2[j]) {
						count++;
					}
				}
			}
//			if (count == 1) {
//				output[i] = count;
//				System.out.println(output[i]);
//			} else if (count == 3) {
//				output[i] = count;
//				System.out.println(output[i]);
//			} else if (count == 4) {
//				output[i] = count;
//				System.out.println(output[i]);
//			}else 
			if (count > 0) {
				output[i] = count;
				// System.out.println(output[i]);
			} else {
				output[i] = 0;
//				System.out.println(output[i]);
			}
		}
		for (int i = 0; i < output.length; i++) {
			System.out.println(output[i]);
		}
	}
}