package inheritanceheirarichal;

public class HeirarichalInheritance {

	public static void main(String[] args) {
		
		ClassB1 classB1=new ClassB1();
		classB1.classB1();
		classB1.classB("Class B1");
		classB1.classA("Class B1");

		ClassB2 classB2=new ClassB2();
		classB2.classB2();
		classB2.classB("Class B2");
		classB2.classA("Class B2");
		
		ClassC classC=new ClassC();
		classC.classC();
		classC.classA("Class C");
		
		ClassD classD=new ClassD();
		classD.classD();
		classD.classA("Class D");

	}

}
