package encapsulation;

public class ClassProtected1 {

	protected String name;

	protected void protectedAS() {
		name = "Class";
		System.out.println("Protected Parent" + name);
	}
	protected void protectedAS1(String cls) {
		System.out.println("Protected Parent  Class Called Using " + cls);
	}

}
