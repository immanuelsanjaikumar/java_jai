package encapsulation;

public class ClassProtected2 extends ClassProtected1 {

	protected String name;

	protected void protectedAS() {
		name = "Class";
		System.out.println("Protected Child" + name);
	}

}
