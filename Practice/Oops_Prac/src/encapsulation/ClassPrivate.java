package encapsulation;

public class ClassPrivate {

	private String name;

	private void privateAS() {
		name = "Class";
		System.out.println("Private " + name);
	}
}
