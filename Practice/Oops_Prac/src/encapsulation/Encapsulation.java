package encapsulation;


public class Encapsulation {

	public static void main(String[] args) {
		
		/*Private ->  Within the Same Class Only Accessible */
		ClassPrivate classPrivate=new ClassPrivate();
		/* classPrivate. (it can't show)*/
		
		/*Default ->  Same Class and Within the same Package */
		ClassDefault classDefault=new ClassDefault();
		String name1=(classDefault.name="Imman");
		System.out.println(name1);
		classDefault.defaultAS();
		
		/*Protected -> Same Class,Sub-Class and Within Same Package*/
		/* Same Class | Same Package*/
		ClassProtected1 classProtected1=new ClassProtected1();
		classProtected1.protectedAS();
		
		/* Sub-Class | Same Package*/
		ClassProtected2 classProtected2=new ClassProtected2();
		classProtected2.protectedAS1("Sub-Class");
		
		/* Public -> It Can be accessed from anywhere within the same project */
		ClassPublic classPublic=new ClassPublic();
		System.out.println((classPublic.name="Imman"));
		classPublic.publicAS();
	}

}
