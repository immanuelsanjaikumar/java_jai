package inheritancemultilvl;

public class ClassB extends ClassA {

	public void classB(String name) {
		System.out.println("Class B Called Using " + name);
	}

}
