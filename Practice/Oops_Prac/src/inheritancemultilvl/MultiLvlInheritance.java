package inheritancemultilvl;

public class MultiLvlInheritance {

	public static void main(String[] args) {
		ClassD classD = new ClassD();
		classD.classD();
		classD.classC("Class D");
		classD.classB("Class D");
		classD.classA("Class D");

	}

}
