package abstraction;


public class Abstraction {

	public static void main(String[] args) {
		
		/* We Can't Able to create obj for abstact class */
		/* AbstractClass abstractClass=new Abstraction(); */
		AbstractClassImpl abstractClassImpl=new AbstractClassImpl();
		abstractClassImpl.name();
		
	}

}
