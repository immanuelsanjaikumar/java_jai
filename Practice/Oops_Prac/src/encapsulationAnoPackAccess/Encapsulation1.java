package encapsulationAnoPackAccess;

import encapsulation.ClassDefault;
import encapsulation.ClassPrivate;
import encapsulation.ClassProtected1;
import encapsulation.ClassPublic;

public class Encapsulation1 {

	public static void main(String[] args) {
		
		/* Private -> it Can't Accessed from another package */
		ClassPrivate classPrivate=new ClassPrivate();
		/* classPrivate. (it can't show)*/
		
		/* Default -> it Can't Accessed from another package */
		ClassDefault classDefault=new ClassDefault();
		/* classDefault. (it can't show)*/
		
		/* Protected -> it Can't Accessed from another package */
		ClassProtected1 classProtected1=new ClassProtected1();
		/* classProtected1. (it can't show)*/
		
		/* Public -> it Can be Accessed from another package */
		ClassPublic classPublic=new ClassPublic();
		System.out.println((classPublic.name="Sonali"));
		classPublic.publicAS();
	}

}
