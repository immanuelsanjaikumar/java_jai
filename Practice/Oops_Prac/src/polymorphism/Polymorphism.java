package polymorphism;


public class Polymorphism {

	public static void main(String[] args) {
		
		/* --- Overloading --- */
		ChildOverloading childOverloading=new ChildOverloading();
		childOverloading.childM1("Child");
		childOverloading.childM1("Using", "Child");
		
		childOverloading.parentM1("Child");
		childOverloading.parentM2("Using", "Child");
		
		/* --- Overriding --- */
		ChildOverriding childOverriding=new ChildOverriding();
		childOverriding.method1();
	}

}
