package polymorphism;


public class ChildOverloading extends ParentOverloading{

	public void childM1(String name) {
		System.out.println("Child 1 Method Overloaded Using "+name);
	}
	
	public void childM1(String using,String name) {
		System.out.println("Child 2 Method Overloaded "+using+" "+name);
	}
}
