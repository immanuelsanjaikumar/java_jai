package polymorphism;


public class ParentOverloading {
	
	public void parentM1(String name) {
		System.out.println("Parent 1 Method Overloaded Using "+name);
	}
	
	public void parentM2(String using,String name) {
		System.out.println("Parent 2 Method Overloaded "+using+" "+name);
	}

}
