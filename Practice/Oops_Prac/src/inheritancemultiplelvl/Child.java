package inheritancemultiplelvl;

public class Child implements Parent1, Parent2 {

	public void child() {
		System.out.println("Child Called");
		Parent1.parent1("Child");
		Parent2.parent1("Child");
	}

}
