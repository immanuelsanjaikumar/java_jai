package association;

public class Association {

	public static void main(String[] args) {
		
		/*--- Aggregation ---*/
		AggregationClassB aggregationClassB = new AggregationClassB();

		// Aggregation weak Relationship Outer Obj dies inner Obj will Exist
		AggregationClassA aggregationClassA = new AggregationClassA(aggregationClassB);

		aggregationClassA.AgrregationMA();

		AggregationClassB aggregationClassB2 = aggregationClassA.getAggregationClassB();

		aggregationClassB2.AgrregationMB();

		aggregationClassA.setName("Imman");
		
		System.out.println(aggregationClassA.getName());
		
		//Referring ClassA Obj to Null
		aggregationClassA = null;
		
		//Null Exception Occurs
		System.out.println(aggregationClassA.getName());
		
		System.out.println("");
		
		
		/*--- Composition ---*/
		// Composition Strong Relationship Outer Obj dies inner Obj also Dies
		CompositionClasssA compositionClasssA=new CompositionClasssA();
		
		compositionClasssA.compositionA();
		
		compositionClasssA.getCompositionClassB().compositionB();
		
		//Referring ClassA Obj to Null
		compositionClasssA=null;
		
		//Null Exception Occurs
		//compositionClasssA.getCompositionClassB().compositionB();
	}

}
