package association;

public class CompositionClasssA {

	private CompositionClassB compositionClassB;

	public void compositionA() {
		System.out.println("Composition Class A Called");
	}

	public CompositionClasssA() {
		this.compositionClassB = new CompositionClassB();
	}

	public CompositionClassB getCompositionClassB() {
		return compositionClassB;
	}

	
}
