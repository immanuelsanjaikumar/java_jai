package association;

public class AggregationClassA {

	private AggregationClassB aggregationClassB;

	private String name;

	public void AgrregationMA() {
		System.out.println("Aggregation Class A Method Called");
	}

	public AggregationClassA(AggregationClassB aggregationClassB) {
		this.aggregationClassB = aggregationClassB;
	}

	public AggregationClassB getAggregationClassB() {
		return aggregationClassB;
	}

	public void setAggregationClassB(AggregationClassB aggregationClassB) {
		this.aggregationClassB = aggregationClassB;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
