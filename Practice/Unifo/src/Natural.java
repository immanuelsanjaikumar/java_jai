


//If we list all the natural numbers below 10 that are multiples of 3 or 5, 
//we get 3, 5, 6 and 9. The sum of these multiples is 23.
//
//Find the sum of all the multiples of 3 or 5 below 1000.
public class Natural {

	public static void main(String[] args) {
		
//		int end=1000;
		int sum=0;
		int sum2=0;
		for (int i = 0; i < 10; i++) {
			if(i%3==0) {
				System.out.println(i);
				sum+=i;
			}
		}
		
		System.out.println("Multiple 3 : "+sum);
		for (int i = 0; i < 10; i++) {
		if(i%5==0) {
			System.out.println(i);
			sum2+=i;
		}
		}
		
		System.out.println("Multiple 5 : "+sum2);
		sum=0;
		sum2=0;
		for (int i = 0; i < 1000; i++) {
			if(i%3==0) {
				System.out.println(i);
				sum+=i;
			}
		}
		
		System.out.println("Multiple 3 : "+sum);
		for (int i = 0; i < 1000; i++) {
			if(i%5==0) {
				System.out.println(i);
				sum2+=i;
			}
			}
		System.out.println("Multiple 5 : "+sum2);

	}

}
