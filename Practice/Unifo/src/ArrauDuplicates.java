import java.util.*;

//a=[23,88,94,72,46,66,90,12]
//b=[88,90,12,70,93,58,23,41]
public class ArrauDuplicates {

	public static void main(String[] args) {
		int[] a = { 23, 88, 94, 72, 46, 66, 90, 12 };
		int[] b = { 88, 90, 12, 70, 93, 58, 23, 41 };
		int combine[] = new int[a.length + b.length];
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			combine[count++] = a[i];
		}
		for (int i = 0; i < b.length; i++) {
			combine[count++] = b[i];
		}

		Set<Integer> RemovedDup = new LinkedHashSet<>();

		for (int val : combine) {
			RemovedDup.add(val);
		}

		Iterator<Integer> iter = RemovedDup.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

	}

}
