public class Reverse {

	public static void main(String[] args) {
		String txt = "Hello World";
		String result = "";
//	for(int i=txt.length()-1;i>=0;i--) {
//		result+=txt.charAt(i);
//	}
//	System.out.println(result);

		String[] splitTxt = txt.split(" ");

		for (int i = 0; i <= splitTxt.length - 1; i++) {
			for (int j = splitTxt[i].length() - 1; j >= 0; j--) {
				result += splitTxt[i].charAt(j);
			}
			if (i < splitTxt.length-1) {
				result += " ";
			}
		}
		System.out.println(result);
	}

}
