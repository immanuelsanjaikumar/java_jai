package fibanacciseries;


public class FibanacciSeriesArray {

	public static void main(String[] args) {
		int arr[] = new int[10];
		int sum;
		for (int i = 0; i <arr.length; i++) {
			if (i > 1) {
				sum=i;
				arr[i]=arr[sum-2]+arr[sum-1];
			}
			else {
				arr[i]=i;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

}
