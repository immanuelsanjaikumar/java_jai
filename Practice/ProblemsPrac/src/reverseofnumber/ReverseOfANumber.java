package reverseofnumber;

public class ReverseOfANumber {

	public static void main(String[] args) {
		int input = 987;
		int remainder = 0;
		int reverse = 0;
		while (input > 0) {
			remainder = input % 10;
			reverse = reverse * 10 + remainder;
			input = input / 10;
		}
		System.out.println(reverse);
	}

}
