package arrays;

public class ArrayWithoutDup {

	public static void main(String[] args) {
		
		int a[] = { 10, 20, 30, 15, 60, 20, 30, 25 };
		int frequency[] = new int[a.length];
		int result[] = new int[a.length];
		int length = 0;
		
		
		for (int i = 0; i < a.length; i++) {
			int store = a[i];
			int count = 1;
			for (int j = i; j < a.length; j++) {
				if (store == a[j]) {
					frequency[i] = count++;
				}
			}
		}

		for (int i = 0; i < a.length; i++) {
			// System.out.println(a[i] + " : " + frequency[i]);
			if (frequency[i] == 1) {
				result[length] = a[i];
				System.out.println(result[length]);
				length++;
			}
		}
	}

}
