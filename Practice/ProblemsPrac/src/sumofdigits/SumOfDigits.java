package sumofdigits;


public class SumOfDigits {

	public static void main(String[] args) {
		int input=1712;
		int result=0;
		while(input>0) {
			result+=input%10;
			input=input/10;
		}
		System.out.println(result);
	}

}
