package armstrongnumber;

import java.util.Scanner;

public class ArmstrongNumber {

	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		int input=sc.nextInt();
		int result=0;
		int temp=input;
		int sum=0;
		int length=(int)Math.log10(input)+1;
		while(temp>0) {
			sum=temp%10;//3 2 1
			sum=(int) Math.pow(sum,length);//27 125 1
			result=result+sum;
			temp=temp/10;//15 1
		}
		
		String rst=(input==result)?"Armstrong Number":"Not Armstrong Number";
		System.out.println(rst);
	}

}
