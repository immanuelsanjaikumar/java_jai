package perfectnumber;


public class PerfectNumber {

	public static void main(String[] args) {
		
		int start=1;
		int end=28;
		int sum=0;
		for (int i = start; i < end; i++) {
				if(end%i==0) {
					sum+=i;
				}
		}
		
		String result=(end==sum)?"Perfect Number":"Not Perfect Number";
		System.out.println(result);
	}

}
