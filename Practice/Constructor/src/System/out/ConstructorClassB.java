package System.out;

import constructor.ConstructorClassA;

public class ConstructorClassB extends ConstructorClassA{

	public ConstructorClassB() {
		System.out.println("Class B Welcome2");
	}

	public ConstructorClassB(String name) {
		super(name);
		System.out.println("Constructor B : "+ name);
	}
}
