package constructor;


public class ConstructorClassA {
	
	public ConstructorClassA() {
		//this("Welcome12");
		System.out.println("Class A Welcome1");
	}

	public ConstructorClassA(String name) {
		System.out.println("Constructor A : "+name);
	}
}
