package prac;

public class NestedForLoop {

	public static void main(String[] args) {
		String input = "Immanam";
		int arr[] = new int[input.length()];
		int k = 0;

		for (int i = 0; i < input.length(); i++) {
			int count = 0;
			for (int j = i + 1; j < input.length(); j++) {
				if (input.charAt(i) == input.charAt(j)) {
					arr[k] = count++;
				}
			}

		}
		
		
		for (int i : arr) {
			System.out.println(i);
		}
	}

}
