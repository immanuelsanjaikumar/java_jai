package prac;


public class DoWhileLoop {

	public static void main(String[] args) {

		int num = 12345;
		int temp=num;
		int count = 0;
		do{
			num=temp%10;//5 4 3 2 1
			System.out.println(count);
			count++;
			temp=temp/10;//1234 123 12 1 0
		}while (temp > 0); 
		System.out.println(count);

	}

}
