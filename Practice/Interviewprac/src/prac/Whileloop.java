package prac;

public class Whileloop {


	public static void main(String[] args) {
		int num = 12345;
		int temp=num;
		int count = 0;
		while (temp > 0) {
			num=temp%10;//5 4 3 2 1
			count++;
			temp=temp/10;//1234 123 12 1 0
		}
		System.out.println(count);

	}

}
