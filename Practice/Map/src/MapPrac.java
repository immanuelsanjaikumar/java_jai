import java.util.*;

public class MapPrac {

	public static void main(String[] args) {
		String txt = "Immanuel 34Sanjai Kumar 12";
		int count = 1;
		Map<Character, Integer> lettertrack = new LinkedHashMap<>();
		for (int i = 0; i < txt.length(); i++) {
			if (lettertrack.containsKey(txt.charAt(i))) {
				lettertrack.replace(txt.charAt(i), lettertrack.get(txt.charAt(i)) + 1);
			} else if (txt.charAt(i) != ' ' && !(txt.charAt(i) >= '0' && txt.charAt(i) <= '9')) {
				lettertrack.put(txt.charAt(i), count);
			}
		}

		for (Map.Entry<Character, Integer> val : lettertrack.entrySet()) {
			System.out.println(val.getKey() + " = " + val.getValue());
		}

	}

}
