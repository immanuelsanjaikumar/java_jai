package comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import comparable.Laptop;

public class ComparatorMain {

	public static void main(String[] args) {
		List<Laptop> laptop = new ArrayList<>();

		laptop.add(new Laptop("Dell", 16, 2000));
		laptop.add(new Laptop("Apple", 8, 1200));
		laptop.add(new Laptop("Acer", 12, 1000));
		
		Comparator<Laptop> comparator=new Comparator<Laptop>() {
			
			@Override
			public int compare(Laptop o1, Laptop o2) {
			int result=o1.getBrand().compareTo(o2.getBrand());
				
				if (o1.getPrice() > o2.getPrice() && result>0 ) {
					return 1;
				} else if (o1.getPrice() < o2.getPrice() && result<0) {
					return -1;
				} else {
					return 0;
				}
			}
		};
		
		Collections.sort(laptop,comparator);
		

		for (Laptop lap : laptop) {
			System.out.println(lap);
		}
	}

}
